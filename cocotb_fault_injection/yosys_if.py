from collections import defaultdict
import hashlib
import logging
import os
import random
import re

from cocotb.log import SimLog

try:
    from pyosys import libyosys as ys
except ModuleNotFoundError as _:
    pyosys_avaliable = False
else:
    pyosys_avaliable = True


def setup_yosys_run_proc_mux(proc_mux):
    AnalyzedRTLDesign._proc_mux = proc_mux

def setup_yosys_set_log_level(log_level):
    AnalyzedRTLDesign._log_level = log_level

class AnalyzedRTLDesign:
    """Singleton class abstracting the yosys interaction, design caching and DFF recognition"""
    _instance = None

    class _AnalyzedRTLDesign:
        _log_level = logging.INFO
        _proc_mux = True
        def __init__(self):
            """Extract FF information using a simple Yosys pass.

            As there is no easy way to look up if a signal is the output of a DFF, we
            go the other way and extract all FFs per module and build a map that can be used
            to look that information up while traversing the simulator hierarchy.
            """
            self._log = SimLog("%s" % (self.__class__.__name__))
            self._log.setLevel(self._log_level)

            if "RTL_SOURCES" not in os.environ:
                raise AttributeError("RTL_SOURCES not exported form Makefile.")
            yosys_rtl_sources = os.environ["RTL_SOURCES"].split()
            
            if "RTL_INCLUDE_DIRS" in os.environ:
                yosys_rtl_includes = os.environ["RTL_INCLUDE_DIRS"].split()
            else:
                yosys_rtl_includes = []
            
            source_string = " ".join(yosys_rtl_sources)
            include_string = " ".join(["-I"+incdir+"" for incdir in yosys_rtl_includes])

            if os.path.isfile("yosys.cache"):
                with open("yosys.cache", 'r') as fd:
                    cache_hash = fd.readline()
            else:
                cache_hash = "invalid"

            hasher = hashlib.md5()
            for source_file in yosys_rtl_sources:
                with open(source_file, 'rb') as fd:
                    hasher.update(fd.read())
            source_hash = hasher.hexdigest()

            if not pyosys_avaliable:
                raise RuntimeError("Yosys python interface (pyosys) not available. Check your Yosys installation.")

            design = ys.Design()
            if source_hash.strip() == cache_hash.strip():
                self._log.info("Loading cached yosys design from previous simulation.")
                ys.run_pass("tee -q -o yosys.log read_ilang yosys.il", design)
            else:
                self._log.info("Running yosys sequential element recognition passes.")
                ys.run_pass("tee -q -o yosys.log read_verilog -mem2reg -noopt %s %s" % (include_string, source_string), design)
                ys.run_pass("tee -q -a yosys.log hierarchy", design)
                ys.run_pass("tee -q -a yosys.log proc_clean", design)
                ys.run_pass("tee -q -a yosys.log proc_rmdead", design)
                ys.run_pass("tee -q -a yosys.log proc_init", design)
                ys.run_pass("tee -q -a yosys.log proc_arst", design)
                if self._proc_mux:
                    ys.run_pass("tee -q -a yosys.log proc_mux", design)
                ys.run_pass("tee -q -a yosys.log proc_dlatch", design)
                ys.run_pass("tee -q -a yosys.log proc_dff", design)
                ys.run_pass("tee -q -a yosys.log proc_clean", design)
                ys.run_pass("tee -q -a yosys.log write_ilang yosys.il", design)

                with open("yosys.cache", 'w') as fd:
                    fd.write("%s\n" % source_hash)

            self._ff_info = defaultdict(list)

            for module in design.selected_whole_modules_warn():
                mod_name = re.search(r"\\[\w]+", module.name.str())
                if mod_name:
                    # FIXME: yosys can also generate an alternate paramod name
                    #  if too many parameters are used
                    #  this would require a more sophisticated regex match
                    mod_name = mod_name[0][1:] # strip leading backslash
                else:
                    raise ValueError("Module name could not be extracted from: %s" % module-name.str())
                for cell in module.selected_cells():
                    if cell.type.str() in ["$dff", "$dffsr", "$adff"]:
                        ff_info = {"q": "", "ctrl": []}
                        q_conn = cell.connections_[ys.IdString("\Q")]
                        if q_conn.is_wire():
                            q_name = q_conn.as_wire().name.str()
                        else:
                            q_name = q_conn.as_chunk().wire.name.str()
                            raise ValueError("Identified DFF-Q is bit-select of vector, which is not supported. Affected signal: %s.%s" % (mod_name, q_name))
                        if q_name.startswith("\\"):
                            ff_info["q"] = q_name[1:]
                            self._log.debug("Identfied DFF in elaborated design: %s.%s" % (mod_name, q_name[1:]))
                        else:
                            raise ValueError("Extracted DFF-Q does not map to RTL: %s.%s" % (mod_name, q_name))

                        if cell.type.str() == "$dffsr":
                            s_name = cell.connections_[ys.IdString("\SET")].as_wire().name.str()
                            r_name = cell.connections_[ys.IdString("\CLR")].as_wire().name.str()
                            s_pol = cell.parameters[ys.IdString("\SET_POLARITY")].as_int()
                            r_pol = cell.parameters[ys.IdString("\CLR_POLARITY")].as_int()

                            if s_name.startswith("\\"):
                                ff_info["ctrl"].append((s_name[1:], s_pol))
                                self._log.debug("Identfied DFF set in elaborated design: %s.%s" % (mod_name, r_name[1:]))
                            else:
                                raise ValueError("Extracted DFF set does not map to RTL: %s.%s" % (mod_name, r_name))

                            if r_name.startswith("\\"):
                                ff_info["ctrl"].append((r_name[1:], r_pol))
                                self._log.debug("Identfied DFF clear in elaborated design: %s.%s" % (mod_name, r_name[1:]))
                            else:
                                raise ValueError("Extracted DFF clear does not map to RTL: %s.%s" % (mod_name, r_name))

                        if cell.type.str() == "$adff":
                            r_name = cell.connections_[ys.IdString("\ARST")].as_wire().name.str()
                            r_pol = cell.parameters[ys.IdString("\ARST_POLARITY")].as_int()
                            if r_name.startswith("\\"):
                                ff_info["ctrl"].append((r_name[1:], r_pol))
                                self._log.debug("Identfied DFF arst in elaborated design: %s.%s" % (mod_name, r_name[1:]))
                            else:
                                raise ValueError("Extracted DFF arst does not map to RTL: %s.%s" % (mod_name, r_name))

                        self._ff_info[mod_name].append(ff_info)

        def get_module_ff_info(self, module_name):
            return self._ff_info[module_name]

    def __init__(self):
        if AnalyzedRTLDesign._instance is None:
            AnalyzedRTLDesign._instance = AnalyzedRTLDesign._AnalyzedRTLDesign()

    def __getattr__(self, name):
        return getattr(self._instance, name)


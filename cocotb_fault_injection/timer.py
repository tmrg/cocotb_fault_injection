import random

from cocotb.simulator import register_timed_callback
from cocotb.triggers import GPITrigger, Trigger
from cocotb.utils import get_sim_steps

class PoissonTimer(GPITrigger):
    """Schedules a timer with exponentially distributed time. Can be used
    for producing more realistic conditions in fault injection.

    Consumes simulation time.
    """
    def __init__(self, mttf, units=None):
        GPITrigger.__init__(self)
        self._mttf = mttf
        self._units = units

    def prime(self, callback):
        duration = random.expovariate(1.0 / self._mttf)
        self.sim_steps = get_sim_steps(max(int(duration), 1), self._units)
        """Register for a timed callback"""
        if self.cbhdl is None:
            self.cbhdl = register_timed_callback(self.sim_steps,
                                                           callback, self)
            if self.cbhdl is None:
                raise_error(self, "Unable set up %s Trigger" % (str(self)))
        Trigger.prime(self, callback)

    def __str__(self):
        return self.__class__.__name__ + "(mttf=%f %s)" % (self._mttf, self._units)

class BoundedRandomTimer(GPITrigger):
    """Schedules a timer with uniformly distributed times between upper and lower
    bounds. Can be used to add randomness to injections while maintaining
    separation between indidual events.

    Consumes simulation time.
    """
    def __init__(self, mttf_min, mttf_max, units=None):
        GPITrigger.__init__(self)
        self._mttf_min = mttf_min
        self._mttf_max = mttf_max
        self._units = units

    def prime(self, callback):
        duration = random.randint(self._mttf_min, self._mttf_max)
        self.sim_steps = get_sim_steps(max(int(duration), 1), self._units)
        """Register for a timed callback"""
        if self.cbhdl is None:
            self.cbhdl = register_timed_callback(self.sim_steps,
                                                           callback, self)
            if self.cbhdl is None:
                raise_error(self, "Unable set up %s Trigger" % (str(self)))
        Trigger.prime(self, callback)

    def __str__(self):
        return self.__class__.__name__ + "(mttf=%f %s)" % ((self._mttf_max + self._mttf_min) / 2, self._units)


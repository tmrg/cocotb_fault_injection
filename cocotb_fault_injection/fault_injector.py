import re
import os
import logging
import random

import cocotb
from cocotb.handle import ModifiableObject, RegionObject, NonHierarchyIndexableObject
from cocotb.handle import Force, Release
from cocotb.triggers import GPITrigger, Timer
from cocotb.log import SimLog, SimLogFormatter, SimTimeContextFilter

from .yosys_if import AnalyzedRTLDesign
from .strategy import InjectionStrategy, _SET, _SEU, _SEE
from .goal import InjectionGoal

class FaultInjector:
    """Class for fault injection into DUTs or parts of DUTs. Not intended for
    directed instantiation by test benches, as no way of actually creating
    the list of SEU/SET signals is implemented here.
    Implements only the actual injection logic and handles the event flow,
    metric tracking, logging and environment interface.
    """
    def __init__(self,
                 mttf_timer=None,
                 transient_duration_timer=None,
                 injection_strategy=None,
                 injection_goal=None,
                 count_handle=None,
                 name_handle=None,
                 leaf_module_info=None,
                 name=None,
                 log_level=logging.INFO,
                 log_file=None,
                 max_signal_len=128,
                 injection_goal_check=128):
        """
        Arguments:
            mttf_timer : GPITrigger
                Timer providing time between faults
            transient_duration_timer : GPITrigger
                Timer providing duration of injected transients
            injection_strategy : InjectionStrategy
                Injection strategy object
            injection_goal : InjectionGoal
                Injection goal object
            count_handle : SimulatorHandle
                Integer handle in which the current SEE ID can be written
            name_handle : SimulatorHandle
                String handle in which the current SEE string can be written
            leaf_module_info : dict
                Dictionary in which (technology) module SEE information is found
            name : str
                Name of the fault injector, to be visible in log files
            log_level : logging.LEVEL
                Level used for logging.
            log_file : str
                If given, redirects any injection-related messages into specified file
            max_signal_len : int
                Maximum length of signals to inject errors into
                (can workaround integer conversion bugs)
            injection_goal_check : int
                Number of injections after which the injection goal is evaluated
        """
        # set up housekeeping and logging
        if name is not None:
            self._name = name
        else:
            self._name = "default"

        self._log = SimLog("%s.%s" % (self.__class__.__name__, self._name))
        self._log.setLevel(log_level)
        
        self._seelog = SimLog("%s.%s_see" % (self.__class__.__name__, self._name))
        self._seelog.setLevel(log_level)
        if log_file is not None:
            fh = logging.FileHandler(log_file, mode='w')
            fh.addFilter(SimTimeContextFilter())
            fh.setFormatter(SimLogFormatter())
            self._log.addHandler(fh)
            self._seelog.addHandler(fh)
            self._seelog.propagate = False

        self._faults = 0
        self._see_id = 0
        self._max_signal_len = max_signal_len
        self._injection_goal_check = injection_goal_check

        if not isinstance(mttf_timer, GPITrigger):
            raise TypeError("MTTF timer is not a GPITrigger object.")
        if not isinstance(transient_duration_timer, GPITrigger):
            raise TypeError("Transient duration timer is not a GPITrigger object.")
        if not isinstance(injection_strategy, InjectionStrategy):
            raise TypeError("Injection strategy is not an InjectionStrategy object.")
        if not isinstance(injection_goal, InjectionGoal):
            raise TypeError("Injection goal is not an InjectionGoal object.")

        self._mttf_timer = mttf_timer
        self._transient_duration_timer = transient_duration_timer
        self._injection_strategy = injection_strategy
        self._injection_goal = injection_goal

        self._leaf_module_info = leaf_module_info or {}
        self._seu_signals = [] # list of dictionaries with SEU handle information
        self._set_signals = [] # list of simulator handles

        self._count_handle = count_handle
        self._name_handle = name_handle

        self._disabled = False
        if "SEE" not in os.environ or os.environ["SEE"] == "0":
            self._log.info("Fault injection disabled by environment settings.")
            self._disabled = True
            return

        if "NETLIST" not in os.environ or os.environ["NETLIST"] == "0":
            self._rtl = AnalyzedRTLDesign()
        else:
            self._log.info("Netlist simulation - skipping Yosys sequential element recognition.")
            self._rtl = None

        if cocotb.SIM_NAME.lower().startswith("icarus"):
            self._log.warning("Falling back to Read-Modify-Write SETs (prone to race conditions).")
            self._put_set = self._put_set_rmw
            self._unput_set = self._unput_set_rmw
        else:
            self._log.info("Using force/release SET injection.")
            self._put_set = self._put_set_force
            self._unput_set = self._unput_set_release

    def _put_seu_reg(self, see):
        try:
            modval = int(see.signal_spec["handle"].value) ^ (1 << see.signal_index)
            see.signal_spec["handle"].value = modval
        except ValueError:
            self._seelog.warning('Skipped injection because signal (%s) is partially undefined.',
                           see.signal_spec["handle"]._path)

    def _put_seu_prim(self, see):
        see.signal_spec["prim_handle"].value = 1

    def _put_seu(self, see):
        if see.signal_spec["type"] == "reg":     # RTL DFF represented as reg
            self._put_seu_reg(see)
        elif see.signal_spec["type"] == "prim":  # primitive-based DFF
            self._put_seu_prim(see)

    def _put_set_force(self, see):
        if see.signal_handle._range is None:
            if see.signal_index:
                raise RuntimeError("Tried to access nonzero index on noniterable handle.")
            try:
                sig_val = int(see.signal_handle)
            except ValueError:
                self._seelog.warning('Skipped injection because signal (%s) is undefined.',
                               see.signal_handle._path)
                return
            see.signal_handle.value = Force(not sig_val)
        else:
            try:
                sig_val = int(see.signal_handle[see.signal_index])
            except ValueError:
                self._seelog.warning('Skipped injection because signal (%s) is undefined.',
                               see.signal_handle._path)
                return
            see.signal_handle[see.signal_index].value = Force(not sig_val)

    def _unput_set_release(self, see):
        if see.signal_handle._range is None:
            if see.signal_index:
                raise RuntimeError("Tried to access nonzero index on noniterable handle.")
            see.signal_handle.value = Release()
        else:
            see.signal_handle[see.signal_index].value = Release()

    def _put_set_rmw(self, see):
        try:
            sig_val = int(see.signal_handle.value)
        except ValueError:
            self._seelog.warning('Skipped injection because signal (%s) is partially undefined.', see.signal_handle._path)
            return
        see.oldval = sig_val & (1 << see.signal_index)
        see.signal_handle.value = sig_val ^ (1 << see.signal_index)

    def _unput_set_rmw(self, see):
        try:
            sig_val = int(see.signal_handle.value)
        except ValueError:
            self._seelog.warning("Could not recover from injected transient because signal (%s) is partially undefined.", see.signal_handle._path)
            return
        if sig_val & (1 << see.signal_index) != see.oldval:
            see.signal_handle.value = sig_val ^ (1 << see.signal_index)

    @cocotb.coroutine
    def _inject_faults(self, fault_spec):
        """Inject faults into a list of signals

        Injects provided upsets or transient events. Consumes simulation time at the begin of its call.
        DFF memory elements can have either upsets or transients, while all other signals can only
        have transients injected.
        """

        seu_list = []
        set_list = []
        for see_spec in fault_spec:
            if not isinstance(see_spec, _SEE):
                raise TypeError("Invalid Single Event Effect specification provided.")
            if isinstance(see_spec, _SET):
                if len(see_spec.signal_handle) > self._max_signal_len:
                    continue
                set_list.append(see_spec)
            if isinstance(see_spec, _SEU):
                if len(see_spec.signal_spec["handle"]) > self._max_signal_len:
                    continue
                seu_sensitive = True
                for ctl_sig in see_spec.signal_spec["ctrl_handles"]:
                    try:
                        if int(ctl_sig[0].value) == ctl_sig[1]:
                            seu_sensitive = False
                            self._seelog.info("Skipping SEU injection because of active reset/preset signal")
                    except ValueError:
                            self._seelog.info("Undefined reset/preset signal")
                if seu_sensitive:
                    seu_list.append(see_spec)

        # await next event time
        yield self._mttf_timer

        # generate actual event
        self._see_id += 1
        self._faults += len(seu_list) + len(set_list)

        see_id_str = ""
        for see in seu_list:
            see_id_str += "SEU_%s[%d] " % (see.signal_spec["handle"]._path, see.signal_index)
            self._seelog.info('SEE ID %d: SEU in %s[%d].', self._see_id, see.signal_spec["handle"]._path, see.signal_index)
            self._put_seu(see)

        for see in set_list:
            see_id_str += "SET_%s[%d] " % (see.signal_handle._path, see.signal_index)
            self._seelog.info('SEE ID %d: SET in %s[%d].', self._see_id, see.signal_handle._path, see.signal_index)
            self._put_set(see)

        # update SEE count signal if provided
        if self._count_handle is not None:
            self._count_handle.value = self._see_id

        # update SEE count string if provided
        if self._name_handle is not None:
            self._name_handle.value = see_id_str

        yield self._transient_duration_timer

        for see in set_list:
            self._unput_set(see)

    @cocotb.coroutine
    def start(self):
        self._running = True
        if self._disabled:
            return

        self._injection_strategy.initialize(seu_signals=self._seu_signals, set_signals=self._set_signals)

        self._log.info("Starting SEE injection.")
        see_gen = iter(self._injection_strategy)
        while not self._injection_goal.eval(self._faults, len(self._seu_signals) + len(self._set_signals)) and self._running:
            for _ in range(self._injection_goal_check):
                see_list = next(see_gen)
                yield self._inject_faults(see_list)
                if not self._running:
                    break
        self._log.info("Finished SEE injection.")
        self._running = False

    def stop(self):
        self._running = False

    @cocotb.coroutine
    def join(self):
        while self._running:
            yield Timer(1, "us")

    def print_summary(self):
        self._log.info("Injected %d SEEs into %d nodes.", self._faults, len(self._seu_signals) + len(self._set_signals))

class HierarchyFaultInjector(FaultInjector):
    def __init__(self,
                 root,
                 exclude_names=None,
                 exclude_paths=None,
                 exclude_modules=None,
                 **kwargs):
        """
        Generate flat map of nodes in given hierarchy, initialize fault injection logic.

        Arguments:
            root : HierarchyObject
                Root node or list of root nodes of hierarchy to be used for fault injection.
            exclude_names  : list of str
                List of signal names to exclude from fault injection (can be regular expressions)
            exclude_paths : list of str
                List of full handle paths to exclude from fault injection (can be regular expressions)
            exclude_modules : list of str
                List of module names to exclude from fault injection
        """
        super().__init__(**kwargs)
        if self._disabled:
            return

        # set up exclude list
        if exclude_names is not None:
            self._exclude_names = "(" + ")|(".join(exclude_names) + ")"
        else:
            self._exclude_names = "$^"
        
        if exclude_paths is not None:
            self._exclude_paths = "(" + ")|(".join(exclude_paths) + ")"
        else:
            self._exclude_paths = "$^"

        self._exclude_modules = exclude_modules or []

        # generate flat signal list from hierarchy root
        if isinstance(root, RegionObject): # Single design root hierarchy object
            self._traverse_hierarchy(root)
        else: # List of design hierarchy objects
            for root_hier in root:
                self._traverse_hierarchy(root_hier)

    def _traverse_hierarchy(self, hier, name_ovr=None):
        """Recursively traverses the hierarchy beginning at the root node in order to build a list
        of signals in the design.
        These will be matched to the flip flop information collected by yosys to be able to judge
        whether SEUs or SETs will be injected.
        """
        mod_name = name_ovr or hier.get_definition_name()
        for handle in hier:
            if re.match(self._exclude_paths, handle._path):
                continue
            # ModifiableObject needs to be handled first as it inherits from NonHierarchyIndexableObject
            # ModifyableObjects are end nodes into which faults can be injected
            if isinstance(handle, ModifiableObject):
                # Check wether the signal is inside the list of excluded signals
                if re.match(self._exclude_names, handle._name):
                    continue
                is_seq = False
                if self._rtl is not None:  # check for RTL-recognized DFF
                    ff_info = list(filter(lambda ff: ff["q"] == handle._name, self._rtl.get_module_ff_info(mod_name)))
                    if ff_info:
                        ctrl_handles = []
                        is_seq = True
                        for ctrl_sig_name, ctrl_sig_val_active in ff_info[0]["ctrl"]:
                            # FIXME: this could fail if a FF is created from a generate loop
                            #  the implementation would need to get the handle from the hier above
                            ctrl_handles.append((getattr(hier, ctrl_sig_name), ctrl_sig_val_active))
                        self._seu_signals.append({"handle": handle, "ctrl_handles": ctrl_handles.copy(), "type": "reg"})
                self._set_signals.append(handle)
                self._seelog.debug("Module: %s, Signal: %s, Seq: %d" % (mod_name, handle._path, is_seq))
            # RegionObjects are objects which can be used to progress further down the hierarchy
            # They will give us another new module name
            elif isinstance(handle, RegionObject):
                mod_name = handle.get_definition_name()
                if mod_name in self._leaf_module_info:  # a module we have SEU/SET information about, do not descend
                    mod_info = self._leaf_module_info[mod_name]
                    for output_name, output_spec in mod_info.items():
                        out_handle = getattr(handle, output_name)
                        if output_spec["type"] == "seq":
                            prim_handle = getattr(handle, output_spec["seu_path"])
                            ctrl_handles = []
                            for ctrl_sig_path, ctrl_sig_val_active in output_spec["seu_cond"]:
                                ctrl_handles.append((getattr(handle, ctrl_sig_path), ctrl_sig_val_active))
                            self._seu_signals.append({"handle": out_handle, "ctrl_handles": ctrl_handles.copy(), "prim_handle": prim_handle, "type": "prim"})
                        self._set_signals.append(out_handle)
                elif mod_name not in self._exclude_modules:  # any other modules not excluded from SEE injection
                    self._traverse_hierarchy(handle)
            # NonHierarchyIndexableObject are iterables on a module level, but do not possess a module name
            # They can be nested, therefore we stick with recursion and carry over the current module name
            elif isinstance(handle, NonHierarchyIndexableObject):
                self._traverse_hierarchy(handle, name_ovr=mod_name)

class SignalListFaultInjector(FaultInjector):
    def __init__(self, set_signals, seu_signals, **kwargs):
        """
        Arguments:
            signals : list of dict
                List of signals to inject faults into (instead of auto-discovery).
                'ctrl' shall be a tuple of handle and active value of reset/preset control lines
        """
        super().__init__(**kwargs)
        self._seu_signals = seu_signals
        self._set_signals = set_signals

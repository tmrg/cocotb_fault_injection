import abc
import random

class _SEE:
    pass

class _SET(_SEE):
    def __init__(self, signal_handle, signal_index):
        self.signal_handle = signal_handle
        self.signal_index = signal_index
        self.oldval = 0 # used during read-modify-write transient injection

class _SEU(_SEE):
    def __init__(self, signal_spec, signal_index):
        self.signal_spec = signal_spec
        self.signal_index = signal_index

class InjectionStrategy(metaclass=abc.ABCMeta):
    def __init__(self, enable_seu=True, enable_set=True):
        """Empty constructor, provided for base class constructor calls succeeding only ATM"""
        self._enable_seu = enable_seu
        self._enable_set = enable_set
        if not any([self._enable_set, self._enable_seu]):
            raise AttributeError("Invalid injection strategy setup. Need to enable either SET, SEU or both.")
    
    @abc.abstractmethod
    def __iter__(self):
        """Injection strategies implement the iterator protocol"""
        pass

    def initialize(self, seu_signals, set_signals):
        """Initialize the injection strategy for a given list of signals"""
        self._seu_signals = seu_signals
        self._set_signals = set_signals

class SequentialInjectionStrategy(InjectionStrategy):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def __iter__(self):
        while True:
            if self._enable_set:
                for sig_handle in self._set_signals:
                    for index in range(len(sig_handle)):
                        yield [_SET(sig_handle, index)]
            if self._enable_seu:
                for sig_spec in self._seu_signals:
                    for index in range(len(sig_spec["handle"])):
                        yield [_SEU(sig_spec, index)]

def _random_index(inj_sig):
    if isinstance(inj_sig._range, tuple):
        return random.randint(min(inj_sig._range), max(inj_sig._range))
    return 0

class RandomInjectionStrategy(InjectionStrategy):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def __iter__(self):
        if self._enable_seu and self._enable_set:
            rand_max = len(self._seu_signals) + len(self._set_signals) - 1
            rand_thresh = len(self._set_signals)
            while True:
                if random.randint(0, rand_max) < rand_thresh:
                    inj_sig = random.choice(self._set_signals)
                    yield [_SET(inj_sig, _random_index(inj_sig))]
                else:
                    inj_sig = random.choice(self._seu_signals)
                    yield [_SEU(inj_sig, _random_index(inj_sig["handle"]))]
        elif self._enable_set:
            while True:
                inj_sig = random.choice(self._set_signals)
                yield [_SET(inj_sig, _random_index(inj_sig))]
        elif self._enable_seu:
            while True:
                inj_sig = random.choice(self._seu_signals)
                yield [_SEU(inj_sig, _random_index(inj_sig["handle"]))]

from setuptools import setup

setup(
    name='cocotb_fault_injection',
    version='0.1',
    description='Fault injection functionality for cocotb',
    author='Stefan Biereigel',
    author_email='stefan.biereigel@cern.ch',
    packages=['cocotb_fault_injection'],
)

#!/bin/bash

check_results() {
    if grep -iq "failure" results.xml; then
        echo "Failure found in results.xml."
        exit 1
    fi
}

# fail script on error
set -e

# prepare environment
export COCOTB=`cocotb-config --share`

# install package to system
pip install .

# run example
cd example/counter_simple/
make TMR=0
check_results
make clean
make TMR=1
check_results

# propagate return code
exit $?

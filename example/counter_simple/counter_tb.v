`timescale 1 ps / 1 ps
module majorityVoterTB (inA, inB, inC, out, tmrErr);
  parameter WIDTH = 1;
  input   [(WIDTH-1):0]   inA, inB, inC;
  output  [(WIDTH-1):0]   out;
  output                  tmrErr;
  reg                     tmrErr;
  assign out = (inA&inB) | (inA&inC) | (inB&inC);
  always @(inA or inB or inC)
  begin
    if (inA!=inB || inA!=inC || inB!=inC)
      tmrErr = 1;
    else
      tmrErr = 0;
  end
endmodule

module counter_tb (
      input  clk,
      input  rst,
      output [7:0] count
  );

`ifdef VCD
  initial begin
     $dumpfile("counter_tb.vcd");
     $dumpvars(0, counter_tb);
  end
`endif

  integer seu_id = 0;
  string seu_name = "";

`ifdef TMR
  // fanout for clk
  wire  clkA=clk;
  wire  clkB=clk;
  wire  clkC=clk;
  // voter for count
  wire [7:0] countA;
  wire [7:0] countB;
  wire [7:0] countC;
  wire counttmrErr;
  majorityVoterTB #(.WIDTH(8)) countVoter (
    .inA(countA),
    .inB(countB),
    .inC(countC),
    .out(count),
    .tmrErr(counttmrErr)
  );
  // fanout for rst
  wire  rstA=rst;
  wire  rstB=rst;
  wire  rstC=rst;
  counterTMR DUT (
    .clkA(clkA),
    .clkB(clkB),
    .clkC(clkC),
    .countA(countA),
    .countB(countB),
    .countC(countC),
    .rstA(rstA),
    .rstB(rstB),
    .rstC(rstC)
  );
`else
  counter DUT (
    .clk(clk),
    .count(count),
    .rst(rst)
  );
`endif

endmodule

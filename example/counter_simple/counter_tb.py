import cocotb
from cocotb.triggers import Timer, FallingEdge, RisingEdge
from cocotb.clock import Clock
from cocotb.result import TestFailure

import numpy as np
import matplotlib.pyplot as plt
import logging

from cocotb_fault_injection import HierarchyFaultInjector, RandomInjectionStrategy, BoundedRandomTimer, SEEsPerNode

class CounterTestbench:
    def __init__(self, dut):
        self.dut = dut
        self.expected = []
        self.actual = []
    
    @cocotb.coroutine
    def reset(self):
        self.dut.rst <= 1
        yield Timer(1)
        yield RisingEdge(self.dut.clk)
        yield FallingEdge(self.dut.clk)
        self.dut.rst <= 0

    @cocotb.coroutine
    def check_count(self):
        # check monotonicity
        for i in range(32*1024):
            yield RisingEdge(self.dut.clk)
            self.expected.append(i % 256)
            self.actual.append(int(self.dut.count))
            if self.expected[-1] != self.actual[-1]:
                self.dut._log.info("Expected: %d, Actual: %d" %((i % 256), int(self.dut.count)))
                raise TestFailure("Mismatch in output.")

@cocotb.test()
def counter_test(dut):
    dut._log.info("Running test!")

    tb = CounterTestbench(dut)
    clock = cocotb.fork(Clock(dut.clk, 25, units='ns').start())
    
    seugen = HierarchyFaultInjector(
                root=dut.DUT,
                mttf_timer=BoundedRandomTimer(mttf_min=50, mttf_max=100, units="ns"),
                transient_duration_timer=Timer(10, "ns"),
                injection_strategy=RandomInjectionStrategy(),
                injection_goal=SEEsPerNode(100),
                count_handle=dut.seu_id,
                log_level=logging.DEBUG)
    seu = cocotb.fork(seugen.start())

    yield tb.reset()
    yield tb.check_count()

    dut._log.info("Finished!")
    seugen.print_summary()


module counter (
        input clk,
        input rst,
        output [7:0] count
    );

    // tmrg default triplicate

    reg [7:0] count_u;
    reg [7:0] count_next;
    wire [7:0] count_nextVoted = count_next;

    always @* begin
        count_next = count_u + 1;
    end

    always @(posedge clk or posedge rst) begin
        if (rst) begin
            count_u <= 0;
        end else begin
            count_u <= count_nextVoted;
        end
    end

    assign count = count_u;
endmodule

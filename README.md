# Fault injection utilities for cocotb
This repository provides some helper packages for directed and randomized fault injection for hardware designs.

## Approach
Generally, two types of faults can appear in digital designs affected by radiation: Single-Event Transients (SET) and Single-Event Upsets (SEU).
While SETs can alter the output of any net, the circuit recovers and the net returns to the correct state after a short time. In SEUs, memory
elements (registers, RAM cells, ...) alter their state permanently. Additional protection circuitry is required to detect and recover from single
event effects.

In order to verify this additional circuitry, single event upsets need to be injected into the design. Doing this on RTL level is non-trivial, as
the HDL description typically does not directly distinguish which signals are memory elements and which nets are combinational logic. In addition,
asynchronous, level-sensitive inputs to these memory elements (set, clear, reset) have to be recognized and respected to allow representative
simulations.

To overcome this problem, the task is divided into two distint phases:

1. A pass through a verilog synthesizer, transforming the RTL code into a representation in which registers and their control signals are recognized.
2. Using this representation to inject the correct type of fault into corresponding nets, depending on the circuit state

For the first phase, the open source 'yosys' synthesis suite is used. Its design is then available via a Python API. The cocotb-component then traverses
the hierarchy of the design, annotating each design node with the information provided by yosys. Afterwards, the fault injection can inject the correct
type of faults into each design node.

An additional advantage of this approach is that it can be applied in the same way to RTL code and gate level netlists. As long as the
gate level netlist is available in verilog form, yosys should be able to recognize memory elements and correctly inject SEUs into them.

![cocotb-fault-injection working principle](images/Fault-Injection.svg)

## Installation
The package itself can be installed using pip.

```
pip install cocotb
git clone ssh://git@gitlab.cern.ch:7999/ADPLL/cocotb-seu-sim.git
pip install -e cocotb-seu-sim/
```

It depends on an installation of yosys with pyosys (the yosys Python bindings) being enabled. Currently, compilation of these bindings is achieved
using Makefile.conf. For ArchLinux, an AUR package with the required configuration is available [here](https://aur.archlinux.org/packages/yosys-git/).

```
make config-gcc
echo "ENABLE_LIBYOSYS=1" >> Makefile.conf
echo "ENABLE_PYOSYS=1" >> Makefile.conf
make
make install
```

## Usage
The component is compatible with the general cocotb Makefile flow. In order to correctly use the fault injection framework, one has to:

1. Define all RTL sources (used in the synthesized design) in a `RTL_SOURCES` variable in the Makefile.
2. Define all verification sources (used for test benches) in a `VRF_SOURCES` variable in the Makefile.
3. Define all required include directories in a `RTL_INCLUDE_DIRS` variable in the Makefile
4. Compose the `VERILOG_SOURCES` variable from `$(RTL_SOURCES) $(VRF_SOURCES)`.
5. Export the `RTL_SOURCES` and `RTL_INCLUDE_DIRS` variable to make it available to the Python code.
6. Export `SEE` whenever injection of faults shall be enabled.

Following these steps, the cocotb testbench can instantiate a FaultInjector object as described and yield/fork one of its fault injection mechanisms.
This flow is outlined in the provided examples.

## Limitations
Even though the approach used has advantages compared to pure gate-level-netlist injection, there are some drawbacks as well.

### Transient injection
Due to the simulation model used by Verilog, injection of transients is unreliable for some syntactic constructs. Combinatorial logic described using
and unclocked always-blocks will not recover from value changes gracefully. Attempts were made to 'reset' injected transients manually, but this comes with
more race conditions. While VPI defines interfaces to react on Event changes in the simulation, either simulators or cocotb itself do not implement these.
Transient injection can safely be used on netlists that have been pre-processed by yosys (transforming all logic to wire+assign), or by gate level netlists
using only primitives.

### Flip-Flop control signals
Control signals of the inferred registers need to be mapped to RTL signals and more complex constructs are not supported. This usually means that
sensitivity lists for reset / clear / set events should consist of only one signal, not a logical combination of multiple. This one signal itself can
have a more complex assignment, so factoring out this logic to an extra wire is a usable workaround. The tool detects these conditions and notifies the user.

### Flip-Flop output signals
Yosys emits multi-bit-FFs for vectors, which is supported. However, flip flops driving only part of a bus are not supported. This could potentially be fixed
with further checking, but would require changes to the injection process and DFF-RTL mapping.
